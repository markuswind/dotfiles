brew update
brew upgrade

brew install carthage
brew install zsh-syntax-highlighting
brew install mercurial
brew install openssl
brew install wget
brew install zsh-syntax-highlighting

# react native libraries
brew install nvm
brew install flow
brew install watchman

nvm install iojs
nvm alias default iojs
npm install -g react-native-cli

brew cask install dropbox
brew cask install flux
brew cask install gimp
brew cask install google-chrome
brew cask install iterm2
brew cask install slack
brew cask install telegram
brew cask install transmission
